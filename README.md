<h3 align="center">Capture and monitor detailed error logs with nice dashboard and UI</h3>

<br>
<br>

#### Requirements

- [Check Laravel 6 requirements](https://laravel.com/docs/6.x#server-requirements)
- [Check Laravel 7 requirements](https://laravel.com/docs/7.x#server-requirements)

## Installation
    $ composer require hpsweb/bugphix-laravel-sdk


### Application usage    
edit: /app/Exceptions/Handler.php
    
    public function report(Exception $exception)
    {
        if (app()->bound('bugphix') && $this->shouldReport($exception)) {
            app('bugphix')->catchError($exception);
        }

        parent::report($exception);
    }

### Test Command
    $ php artisan bugphix:test
