<?php

namespace HPSWeb\BugphixLaravel;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use HPSWeb\BugphixLaravel\Facades\Bugphix as BugphixFacade;
use Route;
use Log;

class BugphixServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    protected $defaultAdminSlug = 'bugphix';

    public function register()
    {

        // Register Bugphix as global Facade class
        $loader = AliasLoader::getInstance();
        $loader->alias('Bugphix', BugphixFacade::class);
        $this->app->singleton('bugphix', function () {
            return new Bugphix();
        });

        if ($this->app->runningInConsole()) {
            // $this->bugphixPublishable();
            $this->bugphixCommands();
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Publishable resources.
     */
    private function bugphixPublishable()
    {
        $path = dirname(__DIR__) . '/src/Publishable';

        $this->publishes([
            "{$path}/config/bugphix.php" => config_path('bugphix.php'),
        ], 'bugphix-config');
    }

    /**
     * Bugphix Commands
     */

    private function bugphixCommands()
    {
        $this->commands([
            // Commands\InstallCommand::class,
            // Commands\BugphixAssetsSymlink::class,
            Commands\BugphixTestCommand::class,
        ]);
    }
}
